# Entanglement Chess

Also known as Haft Schroedinger Chess (http://antumbrastation.com/haft-schroedinger-chess.html).

Doesn't solve using CSP approach, but rather through elimination. I'm not smart enought to write a proof showing that this approach is valid, but this project seems to behave correctly. 

Note: Any piece can be a pawn as well (i.e. pawn is not a separate piece). Castling and En Passant not supported.

Play Here:
https://entanglement-chess.netlify.app/