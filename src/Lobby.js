import { LobbyClient } from 'boardgame.io/client';
const base64json = require('base64json');

const serverUrl = 'https://entanglement-chess.herokuapp.com';
const lobbyClient = new LobbyClient({ server: serverUrl });
window.lobbyClient = lobbyClient;

const update = async () => {
  const { matches } = await lobbyClient.listMatches('entanglement-chess');
  const displayElement = document.getElementById('display');
  displayElement.textContent = '';
  const table = document.createElement('table');
  for(const match of matches) {
    const tableRow = document.createElement('tr');
    const player1Cell = document.createElement('td');
    if(match.players[0].name) {
      player1Cell.textContent = match.players[0].name;
    }
    else {
      const joinButton = document.createElement('button');
      joinButton.innerText = 'Join White';
      joinButton.dataset.playerID = '0';
      joinButton.dataset.matchID = match.matchID;
      joinButton.onclick = joinHandler;
      player1Cell.appendChild(joinButton);
    }
    const vsCell = document.createElement('td');
    vsCell.innerText = ' vs ';
    const player2Cell = document.createElement('td');
    if(match.players[1].name) {
      player2Cell.textContent = match.players[1].name;
    }
    else {
      const joinButton = document.createElement('button');
      joinButton.innerText = 'Join Black';
      joinButton.dataset.playerID = '1';
      joinButton.dataset.matchID = match.matchID;
      joinButton.onclick = joinHandler;
      player2Cell.appendChild(joinButton);
    }
    const spectateCell = document.createElement('td');
    const joinButton = document.createElement('button');
    joinButton.innerText = 'Spectate';
    joinButton.dataset.playerID = '';
    joinButton.dataset.matchID = match.matchID;
    joinButton.onclick = joinHandler;
    spectateCell.appendChild(joinButton);
    tableRow.appendChild(player1Cell);
    tableRow.appendChild(vsCell);
    tableRow.appendChild(player2Cell);
    tableRow.appendChild(spectateCell);
    table.appendChild(tableRow);
  }
  displayElement.appendChild(table);
}

const joinHandler = async (event) => {
  const data = event.target.dataset;
  let encodedObject = {
    server: serverUrl,
    matchID: data.matchID,
    playerID: data.playerID
  }
  if(data.playerID.length > 0) {
    let { playerCredentials } = await lobbyClient.joinMatch(
      'entanglement-chess',
      data.matchID,
      {
        playerID: data.playerID,
        playerName: document.getElementById('nameInput').value.length > 0 ? document.getElementById('nameInput').value : 'Anonymous',
      }
    );
    encodedObject.credentials = playerCredentials;
  }
  else {
    delete encodedObject.playerID;
  }
  window.location = `${window.origin}?data=${base64json.stringify(encodedObject)}`;
}

const createHandler = async () => {
  await lobbyClient.createMatch('entanglement-chess', { numPlayers: 2 });
  update();
}

const createButton = document.getElementById('create');
createButton.onclick = createHandler.bind(this);

const refreshButton = document.getElementById('refresh');
refreshButton.onclick = update.bind(this);
window.setInterval(update.bind(this), 10000);
update();